# -*- coding: utf-8 -*-

import pandas as pd
from nltk import RegexpTokenizer
from pymorphy2 import MorphAnalyzer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
import os
import pickle

tokenizer = RegexpTokenizer('\w+|[a-z|A-Z|А-Я|а-я]')
morph = MorphAnalyzer()
lemmas_map = {}
TRAIN_DATA_PATH = 'train_data.csv'


def load_pickle(f_path):
    with open(f_path, 'rb') as f:
        return pickle.load(f)


class Chatbot(object):

    def __init__(self):

        if os.path.exists(TRAIN_DATA_PATH):
            self._train_data = pd.read_csv(TRAIN_DATA_PATH, index_col=0)
        else:
            self._train_data = pd.DataFrame(columns=['sample', 'prepared_sample', 'target', 'intent_name'])
            self._train_data['target'] = self._train_data['target'].astype('int8')
        self._vectorizer = TfidfVectorizer()
        self._classifier = LogisticRegression()

    @staticmethod
    def _get_lemma(word):
        try:
            lemma = lemmas_map[word]
        except KeyError:
            lemma = morph.parse(word)[0].normal_form
            lemmas_map[word] = lemma
        return lemma


    def _preprocess(self, sentence):
        return ' '.join([self._get_lemma(word) for word in tokenizer.tokenize(sentence)])

    def _add_data(self, data):
        new_df = pd.DataFrame(columns=['sample', 'prepared_sample', 'target'])
        new_df['sample'] = data['sentences']
        new_df['prepared_sample'] = new_df['sample'].apply(self._preprocess)
        new_df['intent_name'] = data['intent_name']

        intent_names = set(self._train_data['intent_name'].unique().tolist() + new_df['intent_name'].unique().tolist())
        names_to_num = {name: n for n, name in enumerate(intent_names)}

        self._train_data = pd.concat([self._train_data, new_df], axis=0, ignore_index=True)
        self._train_data = self._train_data.drop_duplicates()
        self._train_data['target'] = self._train_data['intent_name'].apply(lambda x: names_to_num[x])

        self._train_data.to_csv(TRAIN_DATA_PATH)

    def fit(self, data):
        self._add_data(data)
        tfs = self._vectorizer.fit_transform(self._train_data['prepared_sample'])
        if len(self._train_data['target'].unique()) > 1:
            self._classifier.fit(tfs, self._train_data['target'])

    def predict(self, utterance):
        transformed_utt = self._vectorizer.transform([self._preprocess(utterance)])
        pred = self._classifier.predict(transformed_utt)[0]
        intent_name = self._train_data[self._train_data['target'] == pred]['intent_name']
        return list(intent_name)[0]

    def answer(self, data):
        # Put your code here.
        pass


if __name__ == "__main__":
    chatbot = Chatbot()
    chatbot.fit({'sentences': ['Привет, меня зову Вова, хе-хе',
                               'Ты-дыщь!',
                               'Приветствую!',
                               'Как дела? Привет!!!',
                               'Привет',
                               'Да что ж это делается то'],
                 'intent_name': 'приветствие',
                 'answer': ['Приветствую!.',
                            'Приветствую! Я бот Виталий.']})
    chatbot.fit({'sentences': ['пока дай молока',
                               'покеда',
                               'прощай'],
                 'intent_name': 'прощание',
                 'answer': ['До свидания.',
                            'Прощайте.']})

    chatbot.predict('Прощайте.')
